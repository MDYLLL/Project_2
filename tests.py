import unittest
import home1607_1

class Testmyprog (unittest.TestCase):

    def test_to_roman(self):
        self.assertEqual(home1607_1.to_roman('300'),'CCC')
        self.assertEqual(home1607_1.to_roman('1956'),'MCMLVI')
        with self.assertRaises(home1607_1.NonValidInput):
            home1607_1.to_roman('6000')
        with self.assertRaises(home1607_1.NonValidInput):
            home1607_1.to_roman('sss')

if __name__=='__main__':
    unittest.main()
