import requests, bs4
from multiprocessing import cpu_count
from concurrent.futures import ThreadPoolExecutor, as_completed
import time


class Webtitles:
    def __init__(self, websites):
        self.websites = websites

    def get_title(self, number):
        content = requests.get(self.websites[number])
        parsing = bs4.BeautifulSoup(content.text, 'html.parser')
        title = parsing.select('title')
        title = title[0].getText()
        return [title, number]

    def get_titles(self):
        n = cpu_count()  # посчитали кол-во ядер
        with ThreadPoolExecutor(max_workers=n) as pool:
            results = [pool.submit(self.get_title, i) for i in range(len(self.websites))]

            titles = {}
            for future in as_completed(results):
                titles[(future.result())[1]] = future.result()[0]
        titles = sorted(titles.items())
        titles_output = []
        for title in titles:
            titles_output.append(title[1])

        return titles_output


a = ['https://python.org', 'https://rbc.ru', 'https://championat.com', 'https://yandex.ru', 'https://meduza.io',
     'https://nn.ru', 'https://booking.com', 'https://cenam.net', 'https://retailer.ru', 'https://e-pepper.ru',
     'https://navalny.com', 'https://sports.ru', 'https://anekdot.ru', 'https://samsung.com', 'https://zercalo.org',
     'https://ya.ru', 'https://mail.ru', 'https://kinopoisk.ru', 'https://kolobox.ru', 'https://gitlab.com']
k=time.time()
b = Webtitles(a)
b = b.get_titles()
for title in b:
    print(title)
print (time.time()-k)