import itertools

l1=[1,2,3]
l2=[4,5]
l3=[6,7]

l=[i for i in itertools.chain(l1, l2, l3)]
print(l)

l1=['hello', 'i', 'write', 'cool', 'code']

l=[i for i in itertools.filterfalse(lambda i:len(i)<5,l1)]
print (l)

l=[i for i in itertools.permutations('password', 4)]
print(l)