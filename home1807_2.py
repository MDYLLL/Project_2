import urllib.request


class Webtitle:
    def __init__(self, website):
        self.website = website

    def __repr__(self):
        content = urllib.request.urlopen(self.website).read()
        content = content.decode()
        start = content.find('<title>')
        stop = content.find('</title>')
        return content[start + 7:stop]


a = 'http://python.org/'
b = Webtitle(a)
print(b)

a = 'http://www.rbc.ru/'
b = Webtitle(a)
print(b)

a = 'http://www.cenam.net/'
b = Webtitle(a)
print(b)
