#задача про format
def format (str0,*args):
    str00='' #составляем результат
    i=0 #считаем кол-во аргументов
    stroki=list() #составляем массив аргументов
    for arg in args:
        stroki.append(arg)
        i=i+1
    n=str0.count('{}') #посчитали, сколько раз встречаются фигурные скобки
    if i!=n:
        return "кол-во аргументов и скобочек не равны"
    j=0 #это номер аргумента по порядку
    for i in str0:
        if i!='{' and i!='}':
            str00=str00+i
        elif i=='{':
            str00=str00+stroki[j]
            j=j+1
        else: #т.е. i='}'
            pass

    return str00

print (format('{}, {}, {}', 'a', 'b','c'))
print (format('Coordinates: {}, {}', '37.4N', '-118.3W'))
print (format('{}, {}, {}', 'a', 'b'))
print (format('{}, {}', 'a', 'b','c'))