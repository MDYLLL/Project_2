class NonValidInput(Exception):
    pass


def to_roman(n):
    try:
        n = int(n)
    except Exception:
        raise NonValidInput

    if n < 1 or n > 5000:
            raise NonValidInput
    else:

        rom = ''
        s = n - n % 1000
        s = int(s / 1000)
        if s == 4:
            rom = 'VM'
        else:
            rom = s * 'M'

        s = n - s * 1000
        s = s - s % 100
        s = int(s / 100)
        if s == 9:
            rom = rom + 'CM'
        elif s >= 5:
            rom = rom + 'D' + (s - 5) * 'C'
        elif s == 4:
            rom = rom + 'CD'
        else:
            rom = rom + 'C' * s

        s = n % 100
        s = s - s % 10
        s = int(s / 10)
        if s == 9:
            rom = rom + 'XC'
        elif s >= 5:
            rom = rom + 'L' + 'X' * (s - 5)
        elif s == 4:
            rom = rom + 'XL'
        else:
            rom = rom + 'X' * s

        s = n % 10
        if s == 9:
            rom = rom + 'IX'
        elif s >= 5:
            rom = rom + 'V' + (s - 5) * 'I'
        elif s == 4:
            rom = rom + 'IV'
        else:
            rom = rom + 'I' * s

        return rom


#n = (input('n='))
#try:
#    a = to_roman(n)
#    print(a)
#except NonValidInput:
#    print('ошибка')

