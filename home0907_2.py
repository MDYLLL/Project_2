import argparse
import os
import datetime

now=datetime.datetime.now()

parser = argparse.ArgumentParser()
parser.add_argument("dirpath", type=str, help="directory to creat")
parser.add_argument('-y', '--year', action='store_const', const=str(now.year), help="name with year")
parser.add_argument('-m', '--month', action='store_const', const=str(now.month), help="name with month")
parser.add_argument('-d', '--day', action='store_const', const=str(now.day), help="name with day")

args = parser.parse_args()

result=args.dirpath+'/'

i=0
if args.year!=None:
    result=result+args.year+'-'
    i=1
if args.month!=None:
    result=result+args.month+'-'
    i=1
if args.day!=None:
    result=result+args.day+'-'
    i=1

if i==0:
    result=result+'unknown1'
result=result[0:-1]

print('Пытаюсь создать папку',result)

try:
    os.makedirs (result)
    print('получилось')
except FileExistsError:
    print('такая папка уже существует')
