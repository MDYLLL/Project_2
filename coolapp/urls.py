from django.conf.urls import url


from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^films/$', views.films, name='films'),

    url(r'^film/(\d+)/$', views.film, name='film'),

    url(r'^film/new/$', views.new,name='new'),

    url(r'film/(\d+)/review/$', views.review,name='review')


]