from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.

class Comment(models.Model):
    review = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)


class Films(models.Model):
    tilte = models.CharField(max_length=60)
    desc = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)], default=5)
    review = models.ManyToManyField(Comment)

    def __repr__(self):
        return 'Название фильма: ' + str(self.tilte)
