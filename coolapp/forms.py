# В нем нужно написать:
from django import forms
from .models import Films, Comment


class FilmForm(forms.ModelForm):
    class Meta:
        model = Films
        fields = ('tilte', 'desc', 'rate')


# поля pub_date и id заполняются сами

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('review',)
