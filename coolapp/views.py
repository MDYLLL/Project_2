from django.http import HttpResponse
from django.shortcuts import render
import sqlite3
from coolapp.models import Films
from coolapp.models import Comment
from coolapp.forms import FilmForm,ReviewForm
from django.shortcuts import redirect


def index(request):
    return render(request, 'coolapp/index.html', {'sitename': 'ABOUT FILMS'})


def detail(request):
    return render(request, 'coolapp/detail.html')


def films(request):
    return render(request, 'coolapp/films.html', {'films': Films.objects.all()})


def film(request, film_id):
    f = Films.objects.get(id=film_id)
    return render(request, 'coolapp/file.html', {'film': f,'rev':f.review.all()})

def new(request):
    if request.method == "POST":
        form = FilmForm(request.POST)
        if form.is_valid():
            film = form.save()
            return redirect('/film/{}'.format(film.id), film=film)
    else:
        return render(request, 'coolapp/new.html', {'form': FilmForm()})

def review(request,filmid):
    if request.method=="POST":
        form=ReviewForm(request.POST)
        if form.is_valid():
            review=form.save()
            r1=review
            f=Films.objects.get(id=filmid)
            f.review.add(r1)
            f.save()

            return redirect('/film/{}'.format(filmid))
    else:
        return render(request, 'coolapp/review.html',{'form':ReviewForm()})