from celery import Celery
import celery
import celery.result
from home08_08 import dairy

app = Celery('tasks', backend='redis://localhost', broker='pyamqp://', result_persistent = True)
app.config_from_object('celeryconfig')

app.conf.beat_schedule = {
    'add-every-20-minutes': {
        'task': 'tasks.add',
        'schedule': 600.0,

    },
}
app.conf.timezone = 'UTC'

@app.task
def add():
    dairy()
    return

