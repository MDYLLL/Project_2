import time
import threading
from multiprocessing import Process
from multiprocessing import cpu_count


def odd_primes(end, start=3):
    primes = []
    for i in range(start, end):
        k = 0
        for j in range(2, int(i / 2)):
            if i % j == 0:
                k = 1
        if k == 0:
            primes.append(i)
        k = 0
    print(primes[-1])
    return primes


print('число процессоров = {}'.format(cpu_count()))

start = time.time()
a = odd_primes(10000)
a = odd_primes(20000, 10000)
a = odd_primes(30000, 20000)
k = int(time.time() - start)

print('Время выполнения при последовательном запуске {} сек.'.format(k))

start = time.time()
thr = list()
for i in range(3):
    thr.append(threading.Thread(target=odd_primes, args=(10000 * i + 10000, i * 10000)))
    a = thr[i].start()

for i in range(3):
    a = thr[i].join()

k = int(time.time() - start)

print('Время выполнения при параллельном запуске {} сек.'.format(k))

procs = []
start = time.time()

for i in range(3):
    proc = Process(target=odd_primes, args=(10000 * i + 10000, i * 10000))
    procs.append(proc)
    proc.start()

for i in range(3):
    proc.join()

k = int(time.time() - start)

print('Время выполнения с использованием multiprocessing - {} сек.'.format(k))
