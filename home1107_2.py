#задача про реализацию функции range
def range (start=0,stop=float('inf'),step=1):
    if stop==float('inf'):
        start,stop=0,start
    a=list()
    i=start
    a.append(i)
    if step==0 or abs(start+step-stop)>abs(start-stop):
        return None
    elif step>0:
        while i<stop-step:
            i=i+step
            a.append(i)
        return a
    else:
        while i>stop-step:
            i=i+step
            a.append(i)
        return a

print(range(10))
print(range(-15,5))
print(range(-5,-15,-2))
print(range(10,3,-1))
print(range(5,3,1))
