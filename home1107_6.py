#итератор простых чисел

class Simple:
    def __init__(self,N):
        self.n=1 #счетчик чисел
        self.a=2 #текущее простое число
        self.max=N #сколько чисел надо выдать

    def __iter__(self):
        return self

    def __next__(self):
        if self.n<=self.max:
          for i in range(self.a,self.a*2): #интервал по постулату Бертрана
                k=0
                for j in range(2,int(i/2)+1):
                    if i%j==0:
                        k=1
                if k==0:
                   self.n=self.n+1
                   self.a=i+1
                   return i
        else:
            raise StopIteration


for i in Simple(25):
    print (i)


