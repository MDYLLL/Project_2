#компоновщик
import time
import math
from threading import Thread


class Sheduler:
    def __init__(self,time):
        self.time=time
        self.periodtocall=list()
        self.funtocall=list()

    def addfun(self,period,fun):
        self.periodtocall.append(period)
        self.funtocall.append(fun)

    def method(self):
        pass

    def run(self): #аргументы - список кортежей из периода и названий функций
        i=0
        naksdvig = 0  # накопленный сдвиг
        if self.time==None:
            self.time=float('inf')
        k=0
        sdvig=0
        z=self.periodtocall
        x=self.funtocall
        while k<=self.time:

            start = time.time()

            for j in range(0,len(self.periodtocall)):
                if k%self.periodtocall[j]==0:
                    a.method=self.funtocall[j]
                    Thread(target=a.method).start()

            stop = time.time()
            sdvig = stop - start  # время выполнения функций
            naksdvig = naksdvig + sdvig  # увеличиваем накопленный сдвиг
            if naksdvig<1:
                k=k+1
                time.sleep(1)
            else:
                k = k + math.trunc(naksdvig)  # учитываем целую часть накопленного сдвига
                naksdvig = naksdvig - math.trunc(naksdvig)

def fun1():
    print('функция, выполняемая каждые 5 секунд. текущее время:',time.ctime())
    time.sleep(7)
    return

def fun2():
    print('функция, выполняемая каждые 15 секунд. текущее время:',time.ctime())
    time.sleep(10)
    return

def fun3():
    print('функция, выполняемая каждые 25 секунд. текущее время:',time.ctime())
    return

a=Sheduler(60)
b=list()

#b.append((5,"fun1"))
#b.append((15,"fun2"))
#b.append((25,"fun3"))
a.addfun(5,fun1)
a.addfun(15,fun2)
a.addfun(25,fun3)
a.run()


