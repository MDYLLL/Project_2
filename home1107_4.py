#копирование директории
import os
import shutil


class Basenode:
    def __init__(self, name):
        self.name = name


class File(Basenode):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return 'file:"{}"'.format(self.name)


class Dir(Basenode):
    def __init__(self, nameofdir):
        self.nameofdir = nameofdir
        self.files = list()

    def __repr__(self):
        output = 'directory:{}('.format(self.nameofdir)
        s = len(self.files)
        if s == 0:
            return output + 'empty' + ')'
        for i in range(0, s):
            if type(self.files[i]) == File:
                output = output + ',' * bool(i > 0 and i < s) + str(self.files[i])
            elif type(self.files[i]) == Dir:
                output = output + ',' * bool(i > 0 and i < s) + repr(self.files[i])
        return output + ')'

    def add(self, smth):
        if self.files == None:
            self.files = list()
        self.files.append(smth)


def razvert(a):  # создаем функцию разворачивания директории
    dir = os.listdir(str(a))  # смотрим, что в директории
    dirbase = Dir(a)
    for i in dir:
        if os.path.isfile(a + i):
            dirbase.add(File(i))
        else:
            dirbase.add(razvert(a + i + '/'))
    return dirbase


def truedir(s):  # вовзращает для /dir1/dir2/dir3/ -> dir3/
    s = s[0:-1]
    a = s.rfind('/')
    s = s[a + 1:] + '/'
    return s

def copyinsidedir(dir0,dir1):
    for i in dir0.files:
        if type(i) == File:
            dir1.add(i)
            c = dir0.nameofdir
            d = i.name
            shutil.copyfile(dir0.nameofdir + i.name, dir1.nameofdir + i.name)
        else:
            # надо добавить только конец длинного имени директории
            j = truedir(str(i.nameofdir))
            dir1.add(i)
            b = i.nameofdir
            c = dir1.nameofdir
            d = dir1.nameofdir + j
            os.makedirs(d)
            d=Dir(d)
            e=razvert(i.nameofdir)
            copyinsidedir(e,d)

def copydir(input, output):
    res=output+truedir(input)
    if os.path.isdir(input)==False:
        print('исходная директория не существует')
        return
    elif os.path.isdir(res)==True:
        print('директория назначения существует')
        return
    else:
        dir0 = razvert(input)  # это будет развернутый список с файлами, поддиректориями и их содержимым
        dir1 = Dir(output + truedir(input))  # то, что мы создаем
        c = str(dir1.nameofdir)
        os.makedirs(c)  # создали локально-корневую директорию
        copyinsidedir(dir0,dir1)
        print('копирование выполнено')
        return

a = "/tmp/test/"
b = "/home/mdy/PycharmProjects/My_project/"
# в задании не указано правило ввода данных, поэтому я сделал для такого формата
copydir(a, b)