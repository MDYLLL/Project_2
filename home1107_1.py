#Задача про деньги
class Money:
    def __init__(self, rub, kop):
        self.rub=rub
        self.kop=kop

    def __repr__(self):
        if self.kop<10:
            strkop='0'+str(self.kop)
        else:
            strkop=str(self.kop)
        return '{},{}'.format(self.rub,strkop)

    def sum(m1,m2):
        a=m1.rub*100+m1.kop
        b=m2.rub*100+m2.kop
        c=a+b
        m3=Money(0,0)
        m3.kop=c%100
        m3.rub=int((c-c%100)/100)
        return m3

    def razn(m1,m2):
        a = m1.rub * 100 + m1.kop
        b = m2.rub * 100 + m2.kop
        c = a - b
        m3 = Money(0, 0)
        m3.kop = c % 100
        m3.rub = int((c - c % 100) / 100)
        return m3

    def srav(m1,m2):
        a = m1.rub * 100 + m1.kop
        b = m2.rub * 100 + m2.kop
        c=a-b
        if c==0:
            print ('суммы равны')
        elif c>0:
            print ('первая больше')
        else:
            print ('первая меньше')

    @property
    def kurs(self):
        a=self.rub*100+self.kop
        a=a/60
        s=Money(0,0)
        s.kop=int(a%100)
        s.rub=int((a-a%100)/100)
        return s

q=Money(90,63)
w=Money(5,3)
print('данные суммы-',q,w)
e=Money.sum(q,w)
print('сумма-',e)
e=Money.razn(q,w)
print('разница-',e)
Money.srav(q,w)
print(q,"рублей это примерно",q.kurs,"долларов по курсу 60 руб./дол.")

