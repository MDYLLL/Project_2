import time

def timeit(f):
    def wrap(n):
       start=time.time()
       f(n)
       stop=time.time()
       print ('Время выполнения:',stop-start,'секунд')
    return wrap

@timeit
def spow(n):
    print (2**n)

n=int (input("какая степень двойки Вас интересует:"))
spow(n)
