from tempfile import mktemp
from os import unlink

class WrapStrToFile:
    def __init__(self):
        # здесь инициализируется атрибут filepath, он содержит путь до файла-хранилища
        self._filepath=mktemp() #эта функция создает временный файл


    @property
    def content(self):
        try:
            a=open(self._filepath,'r')
        except FileNotFoundError:
            return 'Файл еще не существует'
        else:
            b=a.read()
            a.close()
            return b
        # попытка чтения из файла, в случае успеха возвращаем содержимое
        # в случае неудачи возращаем 'Файл еще не существует'

    @content.setter
    def content(self, value):
        a=open(self._filepath,'w')
        a.write(value)
        a.close()

        # попытка записи в файл указанное содержимого

    @content.deleter
    def content(self):
        unlink(self._filepath)
        # удаляем файл, например, через ф-ию unlink либы os


wstf = WrapStrToFile()
print(wstf.content)
wstf.content='test str'
print(wstf.content)
wstf.content = 'text 2'
print(wstf.content)
del wstf.content
print(wstf.content)

